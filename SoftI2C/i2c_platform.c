/******************************************************************************

  Copyright (C), 2021-2022, CDT Co., Ltd.

 ******************************************************************************
  File Name     : i2c_platform.c
  Version       : Initial Draft
  Author        : ZhengJH
  Created       : 2021/8/13
  Last Modified :
  Description   : for special platform
  Function List :
  History       :
  1.Date        : 2021/8/13
    Author      : ZhengJH
    Modification: Created file

******************************************************************************/

/*----------------------------------------------*
 * header file                           *
 *----------------------------------------------*/
#include "i2c_platform.h"

/*----------------------------------------------*
 * external variables                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * external routine prototypes                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * internal routine prototypes                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * project-wide global variables                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * module-wide global variables                                    *
 *----------------------------------------------*/

/*----------------------------------------------*
 * constants                                        *
 *----------------------------------------------*/

/*----------------------------------------------*
 * macros                    *
 *----------------------------------------------*/

/*----------------------------------------------*
 * routines' implementations                    *
 *----------------------------------------------*/

/*****************************************************************************
 Prototype    : I2CDelay
 Description  : tSU is critical point
 Input        : uint8_t us
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2021/7/28
    Author       : ZhengJH
    Modification : Created function

*****************************************************************************/
void I2CDelay(uint8_t us)
{
    //with us delay function of your flatform
    /*
    while(us--)
    {
        delay_us(1);
    }
    */
}

/*****************************************************************************
 Prototype    : I2CDelay
 Description  : Read data establishment time, which is required by some non-standard I2C 
                     protocols after the DEVICE (I2C +1) read address is sent
 Input        : uint8_t us
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2021/7/28
    Author       : ZhengJH
    Modification : Created function

*****************************************************************************/
void ReadTsu(void)
{
    //according to the actual situation, this time is generally not required, that is, set to 0.
    /*
    delay_us(time to set-up);
    */
}

