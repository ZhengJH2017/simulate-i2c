/******************************************************************************

  Copyright (C), 2021-2022,, CDT Co., Ltd.

 ******************************************************************************
  File Name     : i2c_sw.h
  Version       : Initial Draft
  Author        : ZhengJH
  Created       : 2021/7/28
  Last Modified :
  Description   : i2c_sw.c header file
  Function List :
  History       :
  1.Date        : 2021/7/28
    Author      : ZhengJH
    Modification: Created file

******************************************************************************/

#ifndef __I2C_SW_H__
#define __I2C_SW_H__


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */
#include "typedef.h"

extern void I2CDelay(uint8_t us);
extern void I2CReceive(uint8_t *prValue, uint8_t fgAck);
extern uint8_t I2CSend(uint8_t Value);
extern void I2CStart(void);
extern void I2CStop(void);
extern uint8_t ReadRegWithDataLen(uint8_t chip_addr, uint8_t* reg_addr, uint8_t addr_size, uint8_t *pData, uint8_t size);
extern uint8_t WriteRegWithDataLen(uint8_t chip_addr, uint8_t* reg_addr, uint8_t addr_size, uint8_t* Data,  uint8_t size);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __I2C_SW_H__ */
