#include <stdio.h>
#include <stdlib.h>
#include "i2c_platform.h"
#include "i2c_sw.h"

int main(int argc, char *argv[])
{
    unsigned char data[1] = {1};
    WriteRegWithDataLen(0x96, NULL, 0, data, 1);
    ReadRegWithDataLen(0x96, NULL, 0, data, 1);
    system("PAUSE");
    return 0;
}
