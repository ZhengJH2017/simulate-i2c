/******************************************************************************

  Copyright (C), 2021-2022, CDT Co., Ltd.

 ******************************************************************************
  File Name     : typedef.h
  Version       : Initial Draft
  Author        : Yfml
  Created       : 2021/6/09
  Last Modified :
  Description   : type define
  Function List :
  History       :
  1.Date        : 2021/6/09
    Author      : Yfml
    Modification: Created file

 2.Date        : 2021/7/29
    Author      : ZhengJH
    Modification: Created file

******************************************************************************/

/*----------------------------------------------*
 * header file                           *
 *----------------------------------------------*/

/*----------------------------------------------*
 * external variables                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * external routine prototypes                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * internal routine prototypes                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * project-wide global variables                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * module-wide global variables                                    *
 *----------------------------------------------*/

/*----------------------------------------------*
 * constants                                        *
 *----------------------------------------------*/

/*----------------------------------------------*
 * macros                    *
 *----------------------------------------------*/

/*----------------------------------------------*
 * routines' implementations                    *
 *----------------------------------------------*/


#ifndef __TYPEDEF_H__
#define __TYPEDEF_H__

/**************
 platform define
***************/
#define _C51_
//#define _X86_

#ifndef NULL
#define NULL  ((void)0)
#endif

#ifndef TRUE
#define TRUE true
#endif

#ifndef FALSE
#define FALSE false
#endif

/*********************
data type
**********************/
typedef void(*v_func)(void);
typedef void* Handle;

#if defined (_X86_)
#undef uint8_t
typedef unsigned char   uint8_t;      ///< range: 0 .. 255
#undef int8_t
typedef signed char     int8_t;      ///< range: -128 .. +127
#undef uint16_t
typedef unsigned short  uint16_t;     ///< range: 0 .. 65535
#undef int16_t
typedef signed short    int16_t;     ///< range: -32768 .. +32767
#undef uint32_t
typedef unsigned int    uint32_t;     ///< range: 0 .. 4'294'967'295
#undef int32_t
typedef signed int      int32_t;     ///< range: -2'147'483'648 .. +2'147'483'647
#elif defined (_C51_)
#undef uint8_t
typedef unsigned char   uint8_t;      ///< range: 0 .. 255
#undef int8_t
typedef signed char     int8_t;      ///< range: -128 .. +127
#undef uint16_t
typedef unsigned int  uint16_t;     ///< range: 0 .. 65535
#undef int16_t
typedef signed int   int16_t;     ///< range: -32768 .. +32767
#undef uint32_t
typedef unsigned long    uint32_t;     ///< range: 0 .. 4'294'967'295
#undef int32_t
typedef signed long      int32_t;     ///< range: -2'147'483'648 .. +2'147'483'647
#endif


/*********************
bit operation
**********************/
#define BIT(n) (1<<n)

#define BITCPL(value,index) (value^=(1<<index))   //取反指定位
#define BITSET(value,index) (value|=(1<<index))   //置位指定位
#define BITCLR(value,index) (value&=~(1<<index))  //清零指定位
#define BITGET(value,index) (value&(1<<index))    //读取指定位

#define SETBITFIELD(value, index, bit) (bit ? BITSET(value, index) : BITCLR(value, index))
#endif

