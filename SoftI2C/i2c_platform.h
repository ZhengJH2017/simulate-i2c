/******************************************************************************

  Copyright (C), 2021-2022, CDT Co., Ltd.

 ******************************************************************************
  File Name     : i2c_porting.h
  Version       : Initial Draft
  Author        : ZhengJH
  Created       : 2021/7/28
  Last Modified :
  Description   : i2c_sw.c platform porting head file
  Function List :
  History       :
  1.Date        : 2021/7/28
    Author      : ZhengJH
    Modification: Created file

******************************************************************************/

#ifndef __I2C_PLATFORM_H__
#define __I2C_PLATFORM_H__


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

#include "typedef.h"

//i/o mode
#define SCL_MOD_OUT 
#define SDA_MOD_OUT 
#define SDA_MOD_IN 

//clock
#define  SCL       //P01
#define  SCL_H   1//P01 = 1
#define  SCL_L   0//P01 = 0

//data
#define  SDA    0  //P00
#define  SDA_H  1//P00 = 1
#define  SDA_H_IN //P00 = 1
#define  SDA_L   0//P00 = 0
#define  SDA_L_IN //P00 = 0


#define ACK_DELAY  100     // Ack confirmation times

#define NOERR       0
#define ACK_OK     NOERR
#define ACK_FAIL  1
#define START_ACK_FAIL  2
#define WDATA_ACK_FAIL   4

//it is policy-free, add according to usage habit, these two functions can also be used externally
#define CRITICAL_ENTER() 
#define CRITICAL_EXIT()

void I2CDelay(uint8_t us);
void ReadTsu(void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif

