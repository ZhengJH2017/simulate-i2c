/******************************************************************************

  Copyright (C), 2021-2022,, CDT Co., Ltd.

 ******************************************************************************
  File Name     : i2c_sw.c
  Version       : Initial Draft
  Author        : ZhengJH
  Created       : 2021/7/28
  Last Modified :
  Description   : common software simulation i2c driver
  Function List :
              I2CDelay
              I2CReceive
              I2CSend
              I2CStart
              I2CStop
              ReadRegWithDataLen
              WriteRegWithDataLen
  History       :
  1.Date        : 2021/7/28
    Author      : ZhengJH
    Modification: Created file

******************************************************************************/

/*----------------------------------------------*
 * header file                           *
 *----------------------------------------------*/
#include "i2c_platform.h"
#include "typedef.h"
#include "i2c_sw.h"

/*----------------------------------------------*
 * external variables                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * external routine prototypes                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * internal routine prototypes                *
 *----------------------------------------------*/

/*----------------------------------------------*
 * project-wide global variables                  *
 *----------------------------------------------*/

/*----------------------------------------------*
 * module-wide global variables                                    *
 *----------------------------------------------*/

/*----------------------------------------------*
 * constants                                        *
 *----------------------------------------------*/

/*----------------------------------------------*
 * macros                    *
 *----------------------------------------------*/

/*----------------------------------------------*
 * routines' implementations                    *
 *----------------------------------------------*/


/*****************************************************************************
 Prototype    : I2CStart
 Description  : I2C STOP  condition
 Input        : void
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2021/7/28
    Author       : ZhengJH
    Modification : Created function

*****************************************************************************/
void I2CStart(void)
{
    /*
    SDA       ^|__

    SCL        ^^|_
    */
    SDA_MOD_OUT;
    SCL_MOD_OUT;
    SDA_H;
    SCL_H;
    I2CDelay(1); //tSU;STA > 0.26us
    SDA_L;
    I2CDelay(1); //tHD;STA > 0.26us
    SCL_L;
    I2CDelay(2); //tLOW > 0.5us
    //return 1;
}


/*****************************************************************************
 Prototype    : I2CStop
 Description  : I2C STOP  condition
 Input        : void
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2021/7/28
    Author       : ZhengJH
    Modification : Created function

*****************************************************************************/
void I2CStop(void)
{
    /*
    SDA       __|^

    SCL        _|^^
    */
    SCL_MOD_OUT;
    SDA_L;
    SDA_MOD_OUT;
    I2CDelay(1);
    SCL_H;
    I2CDelay(2); //tSU;STO > 0.26us
    SDA_H;
    I2CDelay(2); //tBUF > 0.5us
}

/*****************************************************************************
 Prototype    : I2CSend
 Description  : i2c send one uint8_t data
 Input        : uint8_t Value
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2021/7/28
    Author       : ZhengJH
    Modification : Created function

*****************************************************************************/
uint8_t I2CSend(uint8_t Value)
{
    uint16_t bBitMask = 0x80;

    // step 1 : 8-bit data transmission
    //I2CDelay(1);
    SDA_MOD_OUT;
    while(bBitMask)
    {
        I2CDelay(1); //tVD;DAT > 0.45us
        if(bBitMask & Value)
        {
            SDA_H;
            I2CDelay(1); //tSU;DAT > 50ns
        }
        else
        {
            SDA_L;
            I2CDelay(1); //tSU;DAT > 50ns
        }

        SCL_H;
        I2CDelay(1); //tHIGH > 0.26us
        SCL_L;
        I2CDelay(1); //tHD;DAT (only stand-mode > 5us)
        bBitMask = bBitMask >> 1;
        //I2CDelay(1); //tLOW  > 0.5us  tLOW = tVD + tSU
    }

    // step 2 : slave acknowledge check
    SDA_MOD_IN;
    SDA_H_IN;
    I2CDelay(2); //tVD;ACK > 0.45us critical point
    bBitMask = ACK_DELAY;
    while( --bBitMask && SDA )
    {
        I2CDelay(1);
    }
    I2CDelay(1); //tSU;DAT > 50ns
    SCL_H;
    I2CDelay(1); //tHIGH
    SCL_L;
    I2CDelay(1); //tHD;DAT (only stand-mode > 5us)
    if(bBitMask)
    {
        return ACK_OK;
    }
    else
    {
        return ACK_FAIL;
    }
}

/*****************************************************************************
 Prototype    : I2CReceive
 Description  : i2c receive one uint8_t data
 Input        : uint8_t *prValue
                uint8_t fgAck
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2021/7/28
    Author       : ZhengJH
    Modification : Created function

*****************************************************************************/
void I2CReceive(uint8_t *prValue, uint8_t fgAck)
{
    uint16_t bBitMask = 0x80;

    *prValue = 0;                 // reset data buffer
    //SDA_H;                    // release SDA
    SDA_MOD_IN;
    SDA_H_IN;                      // make sure SDA released
    I2CDelay(2);  //tLOW > 0.5us
    // step 1 : 8-bit data reception
    while(bBitMask)
    {
        I2CDelay(1);  //tSU;DAT > 50ns
        SCL_H;                    // data clock out
        I2CDelay(1);  //tHIGH > 0.26us
        if(SDA)
        {
            *prValue = *prValue | bBitMask;   // Get all data
        }                                   // non-zero bits to buffer
        SCL_L;                            // ready for next clock out
        I2CDelay(1); //tHD;DAT (only stand-mode > 5us)

        bBitMask = bBitMask >> 1;           // shift bit mask & clock i2c_delay
        I2CDelay(1); //tVD;DAT > 0.45us
    }

    // step 2 : acknowledgement to slave
    SDA_MOD_OUT;
    if(fgAck)
    {
        SDA_H;                            // ACK here for Sequential Read
    }
    else
    {
        SDA_L;                            // NACK here (for single uint8_t read)
    }

    I2CDelay(1); //tSU;DAT > 50ns
    SCL_H;
    I2CDelay(1);  //tHIGH > 0.26us
    SCL_L;                    // ready for next clock out
    I2CDelay(1); //tHD;DAT (only stand-mode > 5us)

    I2CDelay(1); //tVD;DAT > 0.45us
}

/*****************************************************************************
 Prototype    : WriteRegWithDataLen
 Description  : common i2c write function
 Input        : uint8_t AS
                uint8_t* reg_addr
                uint8_t addr_size
                uint8_t* Data
                uint8_t size
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2021/7/28
    Author       : ZhengJH
    Modification : Created function

*****************************************************************************/
uint8_t WriteRegWithDataLen(uint8_t chip_addr, uint8_t* reg_addr, uint8_t addr_size, uint8_t* Data,  uint8_t size)
{
    uint8_t i;
    uint8_t tmp, err;
    err = NOERR;

    CRITICAL_ENTER();
    I2CStart();
    //оƬ��ַ
    if(I2CSend(chip_addr))
    {
        err = START_ACK_FAIL;
        goto exit;
    }
    //�Ĵ�����ַ
    for(i = 0; i < addr_size; i++)
    {
        tmp = *(reg_addr + i);
        if(I2CSend(tmp))
        {
            err = WDATA_ACK_FAIL;
            goto exit;
        }
    }
    //�Ĵ�������
    for(i = 0; i < size; i++)
    {
        tmp = *(Data + i);
        if(I2CSend(tmp))
        {
            err = WDATA_ACK_FAIL;
            goto exit;
        }
    }
//ֹͣ
exit:
    I2CStop();
    CRITICAL_EXIT();
    return err;
}

/*****************************************************************************
 Prototype    : ReadRegWithDataLen
 Description  : common i2c read function
 Input        : uint8_t chip_addr
                uint8_t* reg_addr
                uint8_t addr_size
                uint8_t *pData
                uint8_t size
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2021/7/28
    Author       : ZhengJH
    Modification : Created function

*****************************************************************************/
uint8_t ReadRegWithDataLen(uint8_t chip_addr, uint8_t* reg_addr, uint8_t addr_size, uint8_t *pData, uint8_t size)
{
    uint8_t i;
    uint8_t tmp, err;
    //��ʼ
    CRITICAL_ENTER();
    I2CStart();

    err = NOERR;
    if(addr_size)
    {
        if(I2CSend(chip_addr))
        {
            err = START_ACK_FAIL;
            goto exit;
        }
        for(i = 0; i < addr_size; i++)
        {
            tmp = *(reg_addr + i);
            if(I2CSend(tmp))
            {
                err = WDATA_ACK_FAIL;
                goto exit;
            }
        }

        I2CStart();
    }
    //chip_addr��1 ��״̬
    if(I2CSend(chip_addr + 1))
    {
        err = START_ACK_FAIL;
        goto exit;
    }
    ReadTsu();
    for(i = 0; i < size; i++)
    {
        if(i == (size - 1))
            I2CReceive(pData + i, 1);
        else
            I2CReceive(pData + i, 0);
    }

    //ֹͣ
exit:
    I2CStop();
    CRITICAL_EXIT();
    return err;
}
